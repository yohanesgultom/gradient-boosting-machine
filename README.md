# Repeated Buyers Prediction using Gradient Boost Machine​ #

Tim Cepat Lulus @ Kudo CodeFest 2016

## Description

Predicting probability of new customers being repeated buyer on a certain merchant based on 26 features extracted from customer profile & activity log

## Dependency

* Pandas
* Scikit
* Numpy
* Sklearn

## Dataset

IJCAI-15 Dataset (TMall) https://tianchi.shuju.aliyun.com/datalab

## Usage

Preprocessing raw data (extracting features from activity logs)

> Currently we only managed to process partial of the real dataset (700,000 out of 7,000,000 provided). Any suggestions are appreciated 

```

python preprocess.py dataset/train_format2_small.csv 

```

Training and evaluation using training data (cross-validation)

```

python main.py dataset/preprocessed_train_format2_small.csv 

```

Training and prediction (return probability of customer(s) being repeated buyer)

```
python main.py --predict dataset/train_format2_small.csv dataset/preprocessed_test_data1.csv

```