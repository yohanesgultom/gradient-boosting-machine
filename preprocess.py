import pandas as pd
import numpy as np
import sys
import time

# CONST
ACTIVITY_ACTION = {'click': 0, 'add-to-cart': 1, 'purchase': 2, 'add-to-favourite': 3}


def tonparray(list2d):
	for i in range(len(list2d)):
		list2d[i] = np.array(list2d[i])
	return np.array(list2d)


def get_activity_log(dtrain, col="activity_log"):
	rows = []
	for item in dtrain[col]:
		if type(item) is not float:
			row = []
			log = item.split("#")
			for i in range(len(log)):
				row.append(log[i].split(":"))
			rows.append(row)
		else:
			rows.append([])
	return np.array(rows)


def add_statistic_features(activity_logs, user_info):
	cols = [
		'click', 'add-to-favourite', 'add-to-cart', 'purchase',
		'click-avg', 'add-to-favourite-avg', 'add-to-cart-avg', 'purchase-avg',
		'click-std', 'add-to-favourite-std', 'add-to-cart-std', 'purchase-std'
	]

	stat_monthly = {}
	for i, val in enumerate(activity_logs):
		row = user_info.iloc[[i]]
		user_id = int(row['user_id'])
		merchant_id = int(row['merchant_id'])
		# summary
		# if not stat.has_key(user_id):
		# 	stat[user_id] = {}
		# if not stat[user_id].has_key(merchant_id):
		# 	stat[user_id][merchant_id] = [0, 0, 0, 0]
		# monthly
		if not stat_monthly.has_key(user_id):
			stat_monthly[user_id] = {}
		if not stat_monthly[user_id].has_key(merchant_id):
			stat_monthly[user_id][merchant_id] = {}
		for activity in activity_logs[i]:
			# item_id = activity[0]
			# cat_id = activity[1]
			# brand_id = activity[2]
			month = activity[3][:2]
			action_code = int(activity[4])
			# stat[user_id][merchant_id][action_code] += 1
			# monthly
			if not stat_monthly[user_id][merchant_id].has_key(month):
				stat_monthly[user_id][merchant_id][month] = [0, 0, 0, 0]
			stat_monthly[user_id][merchant_id][month][action_code] += 1

	features = []
	user_info = user_info[user_info['label'] != -1]
	for i in range(len(user_info)):
		row = user_info.iloc[[i]]
		try:
			label = int(row['label'])
		except Exception as e:
			label = 0
		if not label == -1:
			user_id = int(row['user_id'])
			merchant_id = int(row['merchant_id'])

			# features_tmp = stat[user_id][merchant_id]
			features_tmp = []

			# monthly
			monthly_list = []
			for month, stats in stat_monthly[user_id][merchant_id].iteritems():
				monthly_list.append(stats)
			monthly_nparray = np.array(monthly_list)

			try:
				features_tmp.append(np.sum(monthly_nparray[:, ACTIVITY_ACTION['click']]))
				features_tmp.append(np.sum(monthly_nparray[:, ACTIVITY_ACTION['add-to-favourite']]))
				features_tmp.append(np.sum(monthly_nparray[:, ACTIVITY_ACTION['add-to-cart']]))
				features_tmp.append(np.sum(monthly_nparray[:, ACTIVITY_ACTION['purchase']]))

				features_tmp.append(np.average(monthly_nparray[:, ACTIVITY_ACTION['click']]))
				features_tmp.append(np.average(monthly_nparray[:, ACTIVITY_ACTION['add-to-favourite']]))
				features_tmp.append(np.average(monthly_nparray[:, ACTIVITY_ACTION['add-to-cart']]))
				features_tmp.append(np.average(monthly_nparray[:, ACTIVITY_ACTION['purchase']]))

				features_tmp.append(np.std(monthly_nparray[:, ACTIVITY_ACTION['click']]))
				features_tmp.append(np.std(monthly_nparray[:, ACTIVITY_ACTION['add-to-favourite']]))
				features_tmp.append(np.std(monthly_nparray[:, ACTIVITY_ACTION['add-to-cart']]))
				features_tmp.append(np.std(monthly_nparray[:, ACTIVITY_ACTION['purchase']]))
			except Exception as e:
				features_tmp = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

			features.append(features_tmp)
	return cols, np.array(features)


def merchantRB(activity_logs, user_info):
	cols = ['merchantRB_Jan', 'merchantRB_Feb', 'merchantRB_Mar', 'merchantRB_Apr', 'merchantRB_May', 'merchantRB_June',
			'merchantRB_July', 'merchantRB_Aug', 'merchantRB_Sept', 'merchantRB_Oct', 'merchantRB_Nov',
			'merchantRB_Dec']
	stat = {}
	for i, val in enumerate(activity_logs):
		row = user_info.iloc[[i]]
		merchant_id = int(row['merchant_id'])
		try:
			label = int(row['label'])
		except Exception as e:
			label = 0
		if label == 1:
			if not stat.has_key(merchant_id):
				stat[merchant_id] = {}
			for activity in activity_logs[i]:
				month = activity[3][:2]
				if not stat[merchant_id].has_key(month):
					stat[merchant_id][month] = 0
				stat[merchant_id][month] += 1

	features = []
	key_merchant = stat.keys()
	user_info = user_info[user_info['label'] != -1]
	for i in range(len(user_info)):
		row = user_info.iloc[[i]]
		try:
			label = int(row['label'])
		except Exception as e:
			label = 0
		merchant_id = int(row['merchant_id'])
		features_tmp = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		if merchant_id in key_merchant:
			stat_tmp = stat[merchant_id]
			for i in stat_tmp.keys():
				features_tmp[int(i) - 1] = stat_tmp[i]
		features.append(features_tmp)
	return cols, np.array(features)


def make_dataframe(nparray, cols):
	return pd.DataFrame(nparray, columns=cols)


if __name__ == "__main__":
	train = pd.read_csv(sys.argv[1])
	target = 'label'
	log = 'activity_log'

	user_info = train.drop(log, axis=1)
	array = get_activity_log(train)

	# generate features
	cols, features = add_statistic_features(array, user_info)
	stat_frame = make_dataframe(features, cols)

	# generate features repeated buyers for each merchant per month
	cols_mrb, features_mrb = merchantRB(array, user_info)
	stat_frame_mrb = make_dataframe(features_mrb, cols_mrb)

	train = train[train.label != -1].drop(log, axis=1).reset_index()
	train = pd.concat([train, stat_frame], axis=1)
	train = pd.concat([train, stat_frame_mrb], axis=1)

	train.age_range[pd.isnull(train.age_range)] = 0
	train.gender[pd.isnull(train.gender)] = 2

	file_name = 'preprocessed_train_data_' + str(time.strftime("%Y%m%d%H%M%S")) + '.csv'
	print 'Preprocessed data saved to ' + file_name
	train.to_csv(file_name, encoding='utf-8')
