import pandas as pd
import numpy as np
from sklearn.ensemble import GradientBoostingClassifier  # GBM algorithm
from sklearn import cross_validation, metrics  # Additional scklearn functions
from sklearn.grid_search import GridSearchCV  # Perforing grid search
import matplotlib.pylab as plt
from matplotlib.pylab import rcParams
import sys
from sklearn.externals import joblib
import time

# CONST
# ACTIVITY_ACTION = {0:'click', 1:'add-to-cart', 2:'purchase',3:'add-to-favourite'}
# SAVED_MODEL_FILENAME = 'gbm' + str(time.strftime("%Y%m%d%H%M%S")) + '.pickle'
# PROCESSED_TRAIN_DATA_FILENAME = 'processed_train_data' + str(time.strftime("%Y%m%d%H%M%S")) + '.csv'

# def tonparray(list2d):
# 	for i in range(len(list2d)):
# 		list2d[i] = np.array(list2d[i])
# 	return np.array(list2d)


# def get_activity_log(dtrain, col="activity_log"):
# 	rows = []
# 	for item in dtrain[col]:
# 		if type(item) is not float:
# 			row = []
# 			log = item.split("#")
# 			for i in range(len(log)):
# 				row.append(log[i].split(":"))
# 			rows.append(row)
# 		else:
# 			rows.append([])
# 	return np.array(rows)


# def add_statistic_features(activity_logs, user_info):
# 	# activity_log = list(dtrain['activity_log'])
# 	cols = ACTIVITY_ACTION.values()
# 	# item_id:category_id:brand_id:time_stamp:action_type
# 	stat = {}
# 	for i, val in enumerate(activity_logs):
# 		row = user_info.iloc[[i]]
# 		user_id = int(row['user_id'])
# 		merchant_id = int(row['merchant_id'])
# 		if not stat.has_key(user_id):
# 			stat[user_id] = {}
# 		if not stat[user_id].has_key(merchant_id):
# 			stat[user_id][merchant_id] = [0, 0, 0, 0]
# 		for activity in activity_logs[i]:
# 			# item_id = activity[0]
# 			# cat_id = activity[1]
# 			# brand_id = activity[2]
# 			month = activity[3][:2]
# 			action_code = int(activity[4])
# 			# if not stat[user_id][merchant_id].has_key(month):
# 			# 	stat[user_id][merchant_id][month] = [0, 0, 0, 0]
# 			stat[user_id][merchant_id][action_code] = stat[user_id][merchant_id][action_code] + 1
#
# 	features = []
# 	for i in range(len(user_info)):
# 		row = user_info.iloc[[i]]
# 		label = int(row['label'])
# 		if not label == -1:
# 			user_id = int(row['user_id'])
# 			merchant_id = int(row['merchant_id'])
# 			features.append(stat[user_id][merchant_id])
# 	return cols, np.array(features)


# def make_dataframe(nparray, cols):
# 	return pd.DataFrame(nparray, columns=cols)


def modelfit(alg, dtrain, predictors, performCV=True, printFeatureImportance=True, cv_folds=5):
	# Fit the algorithm on the data
	test = alg.fit(dtrain[predictors], dtrain['label'])

	# Predict training set:
	dtrain_predictions = alg.predict(dtrain[predictors])
	dtrain_predprob = alg.predict_proba(dtrain[predictors])[:, 1]

	# Perform cross-validation:
	if performCV:
		cv_score = cross_validation.cross_val_score(alg, dtrain[predictors], dtrain['label'], cv=cv_folds, scoring='roc_auc')

	# Print model report:
	print "\nModel Report"
	print "Accuracy : %.4g" % metrics.accuracy_score(dtrain['label'].values, dtrain_predictions)
	print "AUC Score (Train): %f" % metrics.roc_auc_score(dtrain['label'], dtrain_predprob)

	if performCV:
		print "CV Score : Mean - %.7g | Std - %.7g | Min - %.7g | Max - %.7g" % (
			np.mean(cv_score), np.std(cv_score), np.min(cv_score), np.max(cv_score))

		# Print Feature Importance:
		# if printFeatureImportance:
		#     feat_imp = pd.Series(alg.feature_importances_, predictors).sort_values(ascending=False)
		#     feat_imp.plot(kind='bar', title='Feature Importances')
		#     plt.ylabel('Feature Importance Score')
		#     plt.show()

def modelevaluate(alg, dtrain, predictors, performCV=True, printFeatureImportance=True, cv_folds=5):

	# Predict training set:
	dtrain_predictions = alg.predict(dtrain[predictors])
	dtrain_predprob = alg.predict_proba(dtrain[predictors])[:, 1]

	# Perform cross-validation:
	if performCV:
		cv_score = cross_validation.cross_val_score(alg, dtrain[predictors], dtrain['label'], cv=cv_folds, scoring='roc_auc')

	# Print model report:
	print "\nModel Report"
	print "Accuracy : %.4g" % metrics.accuracy_score(dtrain['label'].values, dtrain_predictions)
	print "AUC Score (Train): %f" % metrics.roc_auc_score(dtrain['label'], dtrain_predprob)

	if performCV:
		print "CV Score : Mean - %.7g | Std - %.7g | Min - %.7g | Max - %.7g" % (
			np.mean(cv_score), np.std(cv_score), np.min(cv_score), np.max(cv_score))


if __name__ == "__main__":
	target = 'label'
	log = 'activity_log'

	if sys.argv[1] == '--predict':
		train = pd.read_csv(sys.argv[2])
		test = pd.read_csv(sys.argv[3])
		predictors = [x for x in train.columns if x not in [target, log]]
		gbm0 = GradientBoostingClassifier(random_state=10)
		gbm0.fit(train[predictors], train['label'])

		# Predict training set:
		dtrain_predprob = gbm0.predict_proba(test[predictors])[:, 1]
		print 'Repeated customer probability: ' + str(dtrain_predprob)
	else:
		# rcParams['figure.figsize'] = 12, 4
		# train = pd.read_csv('train_format2.csv')
		train = pd.read_csv(sys.argv[1])

		# user_info = train.drop(log, axis=1)
		# array = get_activity_log(train)
		#
		# # generate features
		# cols, features = add_statistic_features(array, user_info)
		# stat_frame = make_dataframe(features, cols)
		#
		# train = train[train.label != -1].drop(log, axis=1).reset_index()
		# train = pd.concat([train, stat_frame], axis=1)
		#
		# train.age_range[pd.isnull(train.age_range)] = 0
		# train.gender[pd.isnull(train.gender)] = 2

		# print(pd.value_counts(train['gender'].values))
		# print(pd.value_counts(train['age_range'].values))

		# Choose all predictors except target & IDcols
		predictors = [x for x in train.columns if x not in [target, log, "Unnamed: 0"]]

		# if len(sys.argv) > 2:
		# 	saved_model = sys.argv[2]
		# 	gbm0 = joblib.load(saved_model)
		# 	modelevaluate(gbm0, train, predictors)
		# else:
		# gbm0 = GradientBoostingClassifier(random_state=10)
		gbm0 = GradientBoostingClassifier(learning_rate=0.1, n_estimators=65, random_state=10, warm_start=True)
		modelfit(gbm0, train, predictors)
		# save model
		# joblib.dump(gbm0, SAVED_MODEL_FILENAME)
